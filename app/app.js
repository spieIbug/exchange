/**
 * handle vue when user click on button element
 * @param money
 * @param btn
 */
function handleButtonClick(money, btn) {
    if (isSelected(money)) {
        btn.html('<span class="fa fa-check"></span>');
    } else {
        btn.html('<span class="fa fa-remove"></span>');
    }
}
/**
 * handle vue when user click on span element
 * @param btn
 */
function handleSpanClick(btn) {
    if (btn.is('.fa-check')) {
        btn.removeClass('fa-check');
        btn.addClass('fa-remove');
    } else {
        btn.removeClass('fa-remove');
        btn.addClass('fa-check');
    }
}
/**
 * add or remove money to / from selected moneys
 * @param money
 */
function toggleListen(money) {
    var btn = $(event.target);
    if (btn.is('button')) {
        handleButtonClick(money, btn);
    } else {
        handleSpanClick(btn);
    }
    var moneys = getSelectedMoneys();
    var index = moneys.indexOf(money);

    if (isSelected(money)) {
        moneys.splice(index, 1);
    } else {
        moneys.push(money);
    }

    localStorage.setItem("moneys", JSON.stringify(moneys));
}

/**
 * return an array of selected moneys
 * @returns {string}
 */
function getSelectedMoneys() {
    var moneys = localStorage.getItem("moneys") || "[]";
    moneys = JSON.parse(moneys);
    return moneys;
}
/**
 * return true if the money is selected, false else
 * @param money
 * @returns {boolean}
 */
function isSelected(money) {
    var moneys = getSelectedMoneys();
    return !!(moneys.indexOf(money) !== -1);
}
/**
 * check if lastPrice equals lowPrice and money is selected, and then return true, else false
 * notify user by voice, and log action
 * @param lastPrice
 * @param lowPrice
 * @param money
 * @returns {boolean}
 */
function checkAndNotify(lastPrice, lowPrice, money) {
    var LOGGER = "./api/log/";
    if (lastPrice === lowPrice && isSelected(money)) {
        var msg = new SpeechSynthesisUtterance(money + ' wassal');
        window.speechSynthesis.speak(msg);
        $.ajax({
            url: LOGGER + '?money=' + money + '&price=' + lastPrice,
            method: 'GET'
        })
        return true;
    }
    return false;
}
/**
 * return TR content for given params
 * @param cssClass
 * @param money
 * @param lastPrice
 * @param lowPrice
 * @returns {string}
 */
function getTRContent(cssClass, money, lastPrice, lowPrice) {
    var tr = "" +
        "<tr class='" + cssClass + "'>" +
        "<td>";
    if (!isSelected(money)) {
        tr = tr +
            "<button class='btn btn-sm btn-block' onclick=\"toggleListen('" + money + "')\">" +
            "<span class='fa fa-check'></span>" +
            "</button>";
    } else {
        tr = tr +
            "<button class='btn btn-sm btn-block' onclick=\"toggleListen('" + money + "')\">" +
            "<span class='fa fa-remove'></span>" +
            "</button>";
    }


    tr = tr + "</td>" +
        "<td>" + money + "</td>" +
        "<td>" + lastPrice + "</td>" +
        "<td>" + lowPrice + "</td>" +
        "</tr>";
    return tr;
}

(function () {
    'use strict';
    var API = "./api/listen";
    var data = [];

    setInterval(checkExchange, 5000);

    function checkExchange() {
        $.ajax({
            url: API,
            method: 'GET'
        }).done(function (response) {
            data = response.market_data;
            $('#table-container').removeClass('loading');
            parseHTML(data);
        }).fail(function (response) {
            console.error(rejection);
        })
    }

    /**
     * reorder moneys, and make selected first
     * @param moneys
     * @returns {Array}
     */
    function selectedFirst(moneys) {
        var filtered = [];
        if (moneys) {
            for (var i = 0; i < moneys.length; i++) {
                if (isSelected(moneys[i].name)) {
                    filtered.push(moneys[i]);
                }
            }
            for (var i = 0; i < moneys.length; i++) {
                if (!isSelected(moneys[i].name)) {
                    filtered.push(moneys[i]);
                }
            }
        }
        return filtered;
    }

    function parseHTML() {
        var table = "<table class='table table-hover'>" +
            "<tr>" +
            "<th>&nbsp;</th>" +
            "<th>name</th>" +
            "<th>last price</th>" +
            "<th>low</th>" +
            "</tr>";

        var filtered = selectedFirst(data);

        for (var i = 0; i < filtered.length; i++) {
            var cssClass = "";
            var moneys = localStorage.getItem("moneys") || "[]";
            moneys = JSON.parse(moneys);
            var money = filtered[i].name;
            var lastPrice = filtered[i].last_price;
            var lowPrice = filtered[i].low;

            if (checkAndNotify(lastPrice, lowPrice, money)) {
                cssClass = "table-success";
            }
            var tr = getTRContent(cssClass, money, lastPrice, lowPrice);

            table = table + tr;
        }
        table = table + "</table>";
        $("#table-container").html(table);
    }

})();
